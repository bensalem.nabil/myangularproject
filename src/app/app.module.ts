import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BlogPostTileComponent } from './blog-post-tile/blog-post-tile.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogPaginationComponent } from './blog-pagination/blog-pagination.component';
import { FiltrePipe } from './filtre.pipe';
import {BlogDataService} from './service/blog-data.service';
import { HightLightDirective } from './directive/hight-light.directive';

@NgModule({
  declarations: [
    AppComponent,
    BlogPostTileComponent,
    BlogListComponent,
    BlogPaginationComponent,
    FiltrePipe,
    HightLightDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [FiltrePipe,BlogDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
