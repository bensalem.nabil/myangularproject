import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {element} from 'protractor';

@Directive({
  selector: '[appHightLight]'
})
export class HightLightDirective {
  @Input() color: string;

  constructor(private  element: ElementRef) {
  }


  @HostListener('mouseenter') addHightLight(){
    this.element.nativeElement.style.backgroundColor = this.color ;
  }

  @HostListener('mouseleave') removeHightLight(){
    this.element.nativeElement.style.backgroundColor = null;
  }
}
