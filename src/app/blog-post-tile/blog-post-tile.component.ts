import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {BlogPost} from '../model/blog-post';

@Component({
  selector: 'app-blog-post-tile',
  templateUrl: './blog-post-tile.component.html',
  styleUrls: ['./blog-post-tile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogPostTileComponent implements OnInit {

  @Input() blog: BlogPost;
  constructor() { }

  ngOnInit() {}

    onShowText(){
      this.blog.summary += ' A ';
      console.log(this.blog.summary);
  }

  onMakeFavorite(){
    this.blog.isFav = !this.blog.isFav;
  }


}
