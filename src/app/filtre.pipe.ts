import { Pipe, PipeTransform } from '@angular/core';
import index from '@angular/cli/lib/cli';

@Pipe({
  name: 'filtre'
})
export class FiltrePipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (value instanceof Array) {
     return  value.filter((value1, index1) => {
       let perPage : number = args[0];
       const currentPage: number = args[1];
       return index1 >= (currentPage -1) * perPage && index1 < (currentPage ) * perPage  ;
     });
    }else {
      return value;
    }
  }


}
