import { Injectable } from '@angular/core';
import {BlogPost} from '../model/blog-post';

@Injectable({
  providedIn: 'root'
})
export class BlogDataService {

  constructor() { }

  getData() : BlogPost[]{
    let blogPosts: BlogPost[] = [];
    for (let _i = 1; _i < 12; _i++) {
      blogPosts.push( new BlogPost('Post ' + _i, 'sammary ' + _i ));
    }
    return blogPosts;
  }
}
