import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-blog-pagination',
  templateUrl: './blog-pagination.component.html',
  styleUrls: ['./blog-pagination.component.scss']
})
export class BlogPaginationComponent implements OnInit {

  @Input() nbPagination: number;
  @Output() pageNbrCLick = new EventEmitter<number>();
  nbPaginationArray: number[] = [];

  constructor() { }

  ngOnInit() {
    this.nbPaginationArray = new Array(this.nbPagination);
  }
  onPageClickedEmit(pageNumber:number){
    this.pageNbrCLick.emit(pageNumber);
  }

}
