import {Component, OnChanges, OnDestroy, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren} from '@angular/core';
import {BlogPost} from '../model/blog-post';
import {BlogPostTileComponent} from '../blog-post-tile/blog-post-tile.component';
import {BlogDataService} from '../service/blog-data.service';
import {FiltrePipe} from '../filtre.pipe';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {

  @ViewChildren('tile') blogPostTileComponent: QueryList<BlogPostTileComponent>;
  blogPosts: BlogPost[] = [];
  nbrPages: number;
  currentPage = 1;
  perPage = 3;
  constructor(private blogService: BlogDataService,private filtrePipe: FiltrePipe) {

  }

  ngOnInit() {
    this.blogPosts = this.blogService.getData();
    this.nbrPages =  this.blogPosts.length / this.perPage;
    this.nbrPages = Math.ceil( this.nbrPages );
    console.log(this.nbrPages);
  }
  onUpdatePageEvent(pageSelected){
    this.currentPage = pageSelected;
  }
  onCallChild(){
    this.blogPostTileComponent.forEach(post => post.onShowText());
  }
  onCallChildFavorite(){
    this.blogPosts.filter((value, index, array) =>{
      return index >= (this.currentPage -1) * this.perPage && index < (this.currentPage ) * this.perPage  ;
    } ).forEach(value => value.isFav = !value.isFav)

  }


}
